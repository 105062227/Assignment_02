var tmp=[];
var point=0;
var main_game={
	preload:function(){
		var n=0;
		firebase.database().ref("/").once('value').then(function(s){
			s.forEach(function(c){
				var k=c.val().player;
				var s;
				for(var i=0;i<k.length-3;++i){
					if(k[i]=='|'&&k[i+1]=='|'&&k[i+2]=='|'&&k[i+3]=='|'){
						s=i;
						break;
					}
				}
				var na=k.slice(0,s);
				var po=k.slice(s+4,k.length);
				tmp[n++]=[na,Number(po)];
			});
		});
		game.load.crossOrigin = 'anonymous';
		game.load.spritesheet('player', 'assets/player.png', 32, 32);
		game.load.image('wall', 'assets/wall.png');
		game.load.image('ceiling', 'assets/ceiling.png');
		game.load.image('normal', 'assets/normal.png');
		game.load.image('nails', 'assets/nails.png');
		game.load.image('pixel', 'assets/pixel.png');
		game.load.image('blood','assets/blood.png');
		game.load.image('gray','assets/gray.png');
		game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
		game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
		game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
		game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
		game.load.audio('bonuce','assets/bonuce.wav');
		game.load.audio('slide','assets/slide.wav');
		game.load.audio('dead','assets/dead.wav');
		game.load.audio('hurt','assets/hurt.wav');
	},
	create:function(){
		this.keyboard = game.input.keyboard.addKeys({
			'enter': Phaser.Keyboard.ENTER,
			'up': Phaser.Keyboard.UP,
			'down': Phaser.Keyboard.DOWN,
			'left': Phaser.Keyboard.LEFT,
			'right': Phaser.Keyboard.RIGHT,
			'w': Phaser.Keyboard.W,
			'a': Phaser.Keyboard.A,
			's': Phaser.Keyboard.S,
			'd': Phaser.Keyboard.D
		});
		this.bonuce=game.add.audio('bonuce');
		this.slide=game.add.audio('slide');
		this.dead=game.add.audio('dead');
		this.hurt=game.add.audio('hurt');
		
		game.stage.backgroundColor = "rgb(66, 241, 244)";
		this.leftWall = game.add.sprite(0, 0, 'wall');
		game.physics.arcade.enable(this.leftWall);
		this.leftWall.body.immovable = true;

		this.rightWall = game.add.sprite(383, 0, 'wall');
		game.physics.arcade.enable(this.rightWall);
		this.rightWall.body.immovable = true;

		this.ceiling = game.add.image(0, 0, 'ceiling');

		this.create_player();
		this.status='running';
		this.distance=0;
		//this.create_emit();
		
		var style = {fill:'#cc0099',fontSize: '20px',align:'center'};
		this.text1 = game.add.text(20, 15, '', style);
		this.text2 = game.add.text(320, 15, '', style);
		this.text3 = game.add.text(140, 200, 'Enter 重新開始', style);
		this.text3.visible = false;
	},
	create_player:function(){
		this.player = game.add.sprite(200, 50, 'player');
		this.player.direction = 10;
		this.player.anchor.setTo(0.5, 0.5);
		game.physics.arcade.enable(this.player);
		this.player.body.gravity.y = 400;
		this.player.animations.add('left', [0, 1, 2, 3], 8);
		this.player.animations.add('right', [9, 10, 11, 12], 8);
		this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
		this.player.animations.add('flyright', [27, 28, 29, 30], 12);
		this.player.animations.add('fly', [36, 37, 38, 39], 12);
		this.player.life = 10;
		this.player.unbeatableTime = 0;
		this.player.touchOn = undefined;
	},
	create_emit:function(){
		this.emitter = game.add.emitter(this.player.x,this.player.y,20);
		this.emitter.makeParticles('blood');
		this.emitter.setYSpeed(-150, 150);
		this.emitter.setXSpeed(-150, 150);
		this.emitter.setScale(1,1,1,1,200);
		this.emitter.gravity = 400;
		this.emitter.start(true,800,null,20);
	},
	create_emit_stair:function(){
		this.emitter = game.add.emitter(this.player.x,this.player.y,15);
		this.emitter.makeParticles('pixel');
		this.emitter.setYSpeed(-150, 150);
		this.emitter.setXSpeed(-150, 150);
		this.emitter.setScale(1,1,1,1,200);
		this.emitter.gravity = 500;
		this.emitter.start(true,800,null,15);
	},
	create_emit_gray:function(){
		this.emitter = game.add.emitter(this.player.x,this.player.y,12);
		this.emitter.makeParticles('gray');
		this.emitter.setYSpeed(-150, 150);
		this.emitter.setXSpeed(-150, 150);
		this.emitter.setScale(1,1,1,1,200);
		this.emitter.gravity = 200;
		this.emitter.start(true,800,null,3);
	},
	update:function(){
		if(this.status != 'running') return;

		game.physics.arcade.collide(this.player, this.platforms, this.effect,null,this);
		game.physics.arcade.collide(this.player, [this.leftWall, this.rightWall]);
		this.checkTouchCeiling(this.player);
		this.checkGameOver();

		this.updatePlayer();
		this.updatePlatforms();
		this.updateTextsBoard();

		this.createPlatforms();
	},
	checkTouchCeiling:function(){
		if(this.player.body.y < 0) {
			if(this.player.body.velocity.y < 0) {
				this.player.body.velocity.y = 0;
			}
			if(game.time.now > this.player.unbeatableTime) {
				this.player.life -= 1;
				game.camera.flash(0xff0000, 100);
				this.player.unbeatableTime = game.time.now + 2000;
				this.create_emit();
				if(this.player.life>0)this.hurt.play();
			}
		}
	},
	checkGameOver:function(){
		if(this.player.life <= 0 || this.player.body.y > 500) {
			this.dead.play();
			this.gameOver();
		}
	},
	gameOver:function(){
		//this.text3.visible=true;
		this.platforms.forEach(function(s) {s.destroy()});
		this.platforms = [];
		game.state.start('dead');
	},
	updatePlayer:function(){
		if(this.keyboard.left.isDown) {
			this.player.body.velocity.x = -250;
		} 
		else if(this.keyboard.right.isDown) {
			this.player.body.velocity.x = 250;
		}
		else {
			this.player.body.velocity.x = 0;
		}
		this.setPlayerAnimate(this.player);
	},
	setPlayerAnimate:function(){
		var x = this.player.body.velocity.x;
		var y = this.player.body.velocity.y;

		if (x < 0 && y > 0){this.player.animations.play('flyleft');}
		if (x > 0 && y > 0){this.player.animations.play('flyright');}
		if (x < 0 && y == 0){this.player.animations.play('left');}
		if (x > 0 && y == 0){this.player.animations.play('right');}
		if (x == 0 && y != 0){this.player.animations.play('fly');}
		if (x == 0 && y == 0){this.player.frame = 8;}
	},
	lastTime:Number(0),
	distance:Number(0),
	platforms:Array(),
	updatePlatforms:function(){
		for(var i=0;i<this.platforms.length; i++) {
			var platform = this.platforms[i];
			var max=this.distance;
			if(max>=20)max=20;
			if(Math.random()>0.9&&this.distance>=10){
				platform.body.position.y+=1.5+max/20;
			}
			else platform.body.position.y-=1.5+max/12;
			var p=12;
			if(this.distance>=7){
				if(Math.random()>0.5&&platform.body.position.x>=17)platform.body.position.x -=this.distance/p;
				else if(platform.body.position.x<=287) platform.body.position.x += this.distance/p;
			}
			if(platform.body.position.y <= -20) {
				platform.destroy();
				this.platforms.splice(i, 1);
			}
		}
	},
	createPlatforms:function(){
		if(game.time.now>this.lastTime + 600) {
			this.lastTime = game.time.now;
			this.createOnePlatform();
			this.distance += 1;
			point=this.distance;
		}
	},
	createOnePlatform:function(){
		var platform;
		var x = Math.random()*(400 - 96 - 40) + 20;
		var y = 400;
		var rand = Math.random() * 100;

		if(rand < 13) {
			platform = game.add.sprite(x, y, 'nails');
			game.physics.arcade.enable(platform);
			platform.body.setSize(96, 15, 0, 15);
		}
		else if (rand < 40) {
			platform = game.add.sprite(x, y, 'normal');
		}
		else if (rand < 50) {
			platform = game.add.sprite(x, y, 'conveyorLeft');
			platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
			platform.play('scroll');
		}
		else if (rand < 60) {
			platform = game.add.sprite(x, y, 'conveyorRight');
			platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
			platform.play('scroll');
		}
		else if (rand < 80) {
			platform = game.add.sprite(x, y, 'trampoline');
			platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
			platform.frame = 3;
		}
		else {
			platform = game.add.sprite(x, y, 'fake');
			platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
		}

		game.physics.arcade.enable(platform);
		platform.body.immovable = true;
		this.platforms.push(platform);

		platform.body.checkCollision.down = false;
		platform.body.checkCollision.left = false;
		platform.body.checkCollision.right = false;
	},
	updateTextsBoard:function(){
		this.text1.setText('生命值:' + this.player.life);
		this.text2.setText('樓層' + this.distance);
	},
	effect:function(player,plat){
		if (player.touchOn !== plat&&plat.key !== 'conveyorRight'&&plat.key !== 'conveyorLeft'){
			this.bonuce.play();
		}
		if(plat.key == 'conveyorRight'){
			this.conveyorRightEffect(player, plat);
			this.slide.play();
		}
		if(plat.key == 'conveyorLeft') {
			this.conveyorLeftEffect(player, plat);
			this.slide.play();
		}
		if(plat.key == 'trampoline') {
			this.trampolineEffect(player, plat);
		}
		if(plat.key == 'nails') {
			this.nailsEffect(player, plat);
		}
		if(plat.key == 'normal') {
			this.basicEffect(player, plat);
		}
		if(plat.key == 'fake') {
			this.fakeEffect(player, plat);
		}
	},
	conveyorRightEffect:function(player, platform) {
		player.body.x += 2;
	},
	conveyorLeftEffect:function(player, platform) {
		player.body.x -= 2;
	},
	trampolineEffect:function(player, platform) {
		platform.animations.play('jump');
		player.body.velocity.y = -350;
	},
	nailsEffect:function(player, platform) {
		if (player.touchOn !== platform) {
			player.life -= 2;
			player.touchOn = platform;
			game.camera.flash(0xff0000, 100);
			this.create_emit();
			if(player.life>0)this.hurt.play();
		}
	},
	basicEffect:function(player, platform) {
		if (player.touchOn !== platform) {
			this.create_emit_stair();
			if(player.life < 10) {
				player.life += 1;
			}
			player.touchOn = platform;
		}
	},
	fakeEffect:function(player, platform) {
		if(player.touchOn !== platform) {
			this.create_emit_gray();
			platform.animations.play('turn');
			setTimeout(function() {
				platform.body.checkCollision.up = false;
			}, 100);
			player.touchOn = platform;
		}
	},
	restart:function(){
		this.text3.visible=false;
		this.distance = 0;
		this.create_player();
		this.status = 'running';
	}
}

var start_menu={
	preload:function(){
		game.load.image('wall', 'assets/wall.png');
		game.load.image('ceiling', 'assets/ceiling.png');
		this.keyboard = game.input.keyboard.addKeys({
			'q': Phaser.Keyboard.Q,
			'w': Phaser.Keyboard.W
		});
	},
	create:function(){
		game.stage.backgroundColor="rgb(66, 241, 244)";
		var style = {fill:'#cc0066',fontSize: '22px',align:'center'};
		this.text1=game.add.text(130, 160, '', style);
		this.text2=game.add.text(130, 200, '', style);
		
		this.leftWall = game.add.sprite(0, 0, 'wall');
		this.rightWall = game.add.sprite(383, 0, 'wall');
		this.ceiling = game.add.image(0, 0, 'ceiling');
	},
	update:function(){
		
		this.text1.setText('開始遊玩(按 q)');
		this.text2.setText('離開遊戲(按 w)');
		this.check();
	},
	check:function(){
		if(this.keyboard.q.isDown){
			game.state.start('main');
		};
		if(this.keyboard.w.isDown){
			window.location.href="https://www.youtube.com/";
		};
	}
}
var over_game={
	preload:function(){
		game.load.image('wall', 'assets/wall.png');
		game.load.image('ceiling', 'assets/ceiling.png');
		this.keyboard = game.input.keyboard.addKeys({
			'q': Phaser.Keyboard.Q,
			'w': Phaser.Keyboard.W,
			'e': Phaser.Keyboard.E
		});
		this.input_name=false;
	},
	create:function(){
		game.stage.backgroundColor="rgb(66, 241, 244)";
		
		var style = {fill:'#ff66ff',fontSize: '30px',align:'center'};
		this.textm=game.add.text(70,50, '',style);
		
		
		var style = {fill:'#ff6699',fontSize: '19px',align:'center'};
		var c1=110;
		this.text1=game.add.text(c1, 100, '', style);
		this.text2=game.add.text(c1, 130, '', style);
		this.text3=game.add.text(c1, 160, '', style);
		this.text4=game.add.text(c1, 190, '', style);
		this.text5=game.add.text(c1, 220, '', style);
		
		var c2=260;
		this.text1_=game.add.text(c2, 100, '', style);
		this.text2_=game.add.text(c2, 130, '', style);
		this.text3_=game.add.text(c2, 160, '', style);
		this.text4_=game.add.text(c2, 190, '', style);
		this.text5_=game.add.text(c2, 220, '', style);
		
		var style = {fill:'#cc0066',fontSize: '19px',align:'center'};
		
		this.text6=game.add.text(140, 270, '', style);
		this.text7=game.add.text(140, 300, '', style);
		this.text8=game.add.text(140, 330, '', style);
		//console.log(tmp.length);
		tmp.sort(function(a,b){
			return a[1]<b[1];
		});
		for(var i=tmp.length;i<5;++i){
			tmp[i]=['nobody',0];
		}
		
		this.leftWall = game.add.sprite(0, 0, 'wall');
		this.rightWall = game.add.sprite(383, 0, 'wall');
		this.ceiling = game.add.image(0, 0, 'ceiling');
		
		this.textm.setText('You got '+point.toString()+' points!!!');
		
		this.text1.setText(tmp[0][0].slice(0,12));
		this.text1_.setText(tmp[0][1].toString());
		this.text2.setText(tmp[1][0].slice(0,12));
		this.text2_.setText(tmp[1][1].toString());
		this.text3.setText(tmp[2][0].slice(0,12));
		this.text3_.setText(tmp[2][1].toString());
		this.text4.setText(tmp[3][0].slice(0,12));
		this.text4_.setText(tmp[3][1].toString());
		this.text5.setText(tmp[4][0].slice(0,12));
		this.text5_.setText(tmp[4][1].toString());
		
		this.text6.setText('再玩一次(按 q)');
		this.text7.setText('離開遊戲(按 w)');
		this.text8.setText('上傳成績(按 e)');
	},
	input_name:false,
	update:function(){	
		this.check();
	},
	check:function(){
		if(this.keyboard.q.isDown){
			game.state.start('main');
		};
		if(this.keyboard.w.isDown){
			window.location.href="https://www.youtube.com/";
		};
		if(this.keyboard.e.isDown){
			if(!this.input_name){
				var name=prompt("Your name~~");
				if(name){
					alert('Damn it, your dope!!!');
				}
				firebase.database().ref("/").push({
					player:name+'||||'+point.toString()
				});
				this.input_name=true;
			}
		}
	}
}


var dead_game={
	preload:function(){
		game.load.image('wall', 'assets/wall.png');
		game.load.image('ceiling', 'assets/ceiling.png');
		this.keyboard=game.input.keyboard.addKeys({
			'enter': Phaser.Keyboard.ENTER
		});
	},
	create:function(){
		game.stage.backgroundColor="rgb(66, 241, 244)";
		this.leftWall = game.add.sprite(0, 0, 'wall');
		this.rightWall = game.add.sprite(383, 0, 'wall');
		this.ceiling = game.add.image(0, 0, 'ceiling');
		
		var style = {fill:'#ff3300',fontSize: '32px',align:'center'};
		this.textm=game.add.text(60,100, '',style);
		this.textm.setText('You got '+point.toString()+' points!!!');
		
		var style = {fill:'#ff9900',fontSize: '20px',align:'center'};
		this.text1=game.add.text(130,200,'按ENTER繼續',style);
	},
	update:function(){
		if(this.keyboard.enter.isDown){
			game.state.start('over');
		}
	}
}
var game=new Phaser.Game(400,400,Phaser.AUTO,'canvas');
game.state.add('dead',dead_game);
game.state.add('main',main_game);
game.state.add('menu',start_menu);
game.state.add('over',over_game);
game.state.start('menu');



