# 小朋友下樓梯

### 遊戲截圖
<img src="example01.png" width="414px" height="411px"></img>

### 玩法介紹
* 利用方向鍵的下左右來移動。
* 碰到尖刺或是天花板會扣血。
* 掉出畫面或是血量歸零會結束遊戲。
* 踩到藍色的平台會加一滴血。
* 踩到綠色的平台會向上彈跳。
* 踩到灰色的平台會往下掉落。
* 踩到滾輪型平台會被帶著移動。
* 結束遊戲後可選擇要不要上傳成績，如要上傳，結果將於下一次回到結束畫面時顯示。

### 必要項目
* A complete game process: start menu => game view => game over => quit or play again
    * 有開始選單畫面，遊戲畫面，死亡畫面，結束遊戲畫面。
        * 開始選單可選擇要開始遊戲或是離開頁面。
        * 死亡畫面會顯示分數。
        * 結束畫面可選擇要離開或是再玩一次，也可以在這裡上傳成績。

* Your game should follow the basic rules of “小朋友下樓梯”.
    * 各項規則已經寫在上方的玩法介紹。

* Set up some interesting traps or special mechanisms.(at least 2 different kinds of platform).
    * 滾輪型平台會被帶著移動。
    * 灰色平台會在短暫延遲後往下掉。
    * 綠色平台可以往上跳。
    * 平台會隨著時間開始左右、上下不規則移動。

* The player in your game should have correct physical properties and behaviors.
    * 已使用物理引擎，並設定好地心引力、彈跳等等係數。

* Add some additional sound effects and UI to enrich your game.
    * 跳到平台時會有彈跳音效。
    * 跳到滾輪型平台時會有特殊音效。
    * 受傷扣血時會有尖叫聲。
    * 死亡時會有慘叫聲。
    * 選單、死亡、結束畫面，有經過排版並加入一些物件美化，不至於長得太難看。

* Store player's name and score in firebase real-time database, and add a leaderboard to your game.
    * 結束畫面有排行榜，列出前五高的玩家的名字與成績。
    * 在結束畫面可以選擇上傳成績，若是排在前五名，在下一次進入結束畫面時，便可以看到自己的名字與成績出現在排行榜上。

* Appearance (subjective).

* Other creative features in your game (describe on README.md).
    * 在碰到一般平台時，會掉出藍色的碎屑。
    * 在碰到灰色的假平台時，會掉出灰色的碎屑。
    * 當碰到尖刺或天花板導致扣血時，會噴出紅色的血滴。
    * 隨著時間，平台會開始隨機左右、上下移動，增加遊戲的難度。